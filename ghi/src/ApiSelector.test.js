import { render } from '@testing-library/react' ;
import ApiSelector from './ApiSelector';


test('ApiSelector has div with form classes'), () => {
    //Arrange
    const fake = () => {} ;
    
    // Act
    const {container} = render(<ApiSelector
                                value= "jeff"
                                selected= "mitch"
                                label= "LABEL!"
                                onChange={fake} />)
    const div = container.querySelector('div.text-start.form-check');
    // Assert
    expect(div.toBeTruthy) ;
}

test('ApiSelector has all properties and not checked with values'), ()  => {
    //Arrange
    const fake = () => {} ;
    
    // Act
    const {container} = render(<ApiSelector
                                value= "jeff"
                                selected= "mitch"
                                label= "LABEL!"
                                onChange={fake} />)
    const input = container.querySelector('input.form-check-input');
    // Assert
    expect(input).toBeTruthy() ;
    expect(input.type).toEqual('radio');
    expect(input.value).toEqual('jeff');
    expect(input.name).toEqual('api-selector');
    expect(input.id).toEqual('jeff');
    expect(input.checked).toBeFalsy();
}